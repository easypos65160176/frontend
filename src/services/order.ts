import type { Receipt } from '@/types/Receipt'
import http from './http'
import type { ReceiptItem } from '@/types/ReceiptItem'
type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
  }[]
  userId: number
}
function addOrder(receipt: Receipt, receiptItems: ReceiptItem) {
  const ReceiptDto: ReceiptDto = {
    orderItems: [],
    userId: 0
  }
  ReceiptDto.userId = receipt.userId
  receiptDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.unit
    }
  })
  console.log(ReceiptDto)
  return http.post('/orders', receiptDto)
}
export default { addOrder }
